# Git repository containing the work of a bachelor-thesis about configuration management with Redfish and Ansible.
Contains PoC python scripts, playbooks, roles and modules for OOB management using Redfish API.        It is tested on Dell Poweredge C6420 servers with iDRAC version 4.32.10.00 and demonstrates the following functionality

- Read and store BMC (iDRAC) settings with Redfish (script/command)
- Write settings to BMC(iDRAC)/BIOS (Script/command/playbook)
- Deploying a "default" IDRAC from previous iDRAC (Uses Dell server configuration profiles) Uses custom module for previewing configuration and gathering job details to get indebt information in case of import-failure.
- Change of BMC(iDRAC) iDRAC settings with Redfish and Ansible on one or more devices

## Playbooks and inventory file

The playbooks in [playbooks](playbooks) shows how community modules, custom modules, and roles are used to demonstrate the aforementioned functionality and can be used as a reference when writing playbooks or roles for your own needs.


The [inventory](static_inventory.yml) is shows an example inventory file used in the testing environment.


## PoC development work (Modules)

The [modules](plugins/modules) directory contains two PoC modules for import server configuration profile preview (Dell specific) and gathering details about asynchronous Redfish jobs/tasks (accepts job\_id as parameter).
