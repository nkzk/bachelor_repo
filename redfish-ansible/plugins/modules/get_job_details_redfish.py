#!/usr/bin/python
# Copyright: (c) 2020, 
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: get_job_details_redfish

short_description: Get details about an asynchronous Redfish job/task.

version_added: -

description: This is a PoC module which will return the body of the GET request on the Redfish URI of the task, return value (true or false), and the state of the task (complete, running).

notes:
    - Run this module from a system that direct access to a BMC with implemented Redfish specification > 1.6.0.
    - Tested on DellEMC iDRAC fw version 4.32.10.00.

options:
    ip:
        description: IP address of the OOB controller
        required: true
        type: str
    username:
        description: Username for authentication with the OOB controller
        required: true
        type: str
    password:
        description: Password for authentication with the OOB controller
        required: true
        type: str
    job_id:
        description: Job id of the task to query
        required: true
        type: str
    manager_id:
        description: ID of the OOB manager instance
        required: true
        type: str

'''

EXAMPLES = r'''
# Get details about the job, store return in output
  - name: Get job details
    get_job_details_redfish:
      ip: "{{ ansible_host }}"
      username: "{{ ansible_user }}"
      password: "{{ ansible_password }}"
      job_id "{{testout.job_id }}" # job_id is gotten from previous task that starts a job and returns job_id 
    register: output

# Get details about the job untill state is either failed or complete ( != running). Retry 10 times.
  - name: Get job details
    get_job_details_redfish:
      ip: "{{ ansible_host }}"
      username: "{{ ansible_user }}"
      password: "{{ ansible_password }}"
      job_id: "{{ testout.job_id }}"
    register: output
    until: output.JobState != "Running"
    retries: 10

# Print response in console
  - name: print
    ansible.builtin.debug:
      msg: "{{ output }}"
'''


RETURN = r'''
# Return messages coming soon.
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import open_url
from ansible.module_utils.six.moves.urllib.error import URLError, HTTPError
from ansible.module_utils._text import to_native
import json
from ansible_collections.community.general.plugins.module_utils.redfish_utils import RedfishUtils

class ExtendedRedfishUtils(RedfishUtils): 
     def get_job_id_details(self, job_id):
        result = {} 
        task_uri = "TaskService/Tasks/" + job_id
        full_uri = self.root_uri + task_uri
        response = self.get_request(full_uri)
        if response['ret'] is False:
            result['response'] = "fail url: %s" %full_uri
            result['ret'] = False
            return result
        result['response'] = response
        result['ret'] = response['ret'] 
        result['JobState'] = response['data']['TaskState']
        return result

def run_module():

    # define result dict
    result = {}
    # the AnsibleModule object will be our abstraction working with Ansible
    module = AnsibleModule(
        argument_spec = dict(
            # defaults
            timeout = dict(required=False, type='int', default = 10),
            # credentials
            ip = dict(required=True, type='str'),
            username = dict(required=True, type='str'),
            password = dict(required=True, type='str', no_log=True),
            # module specifics
            job_id = dict(required=True, type='str'),
        ),
        supports_check_mode=False)

    # credentials 
    creds = {'user': module.params['username'],
             'pswd': module.params['password']}
    # Build URL
    root_uri = "https://" + module.params['ip'] + "/redfish/v1/"

    # create RedfishUtils object
    rf_utils = ExtendedRedfishUtils(creds, root_uri, module.params['timeout'],module)     
    result = rf_utils.get_job_id_details(module.params['job_id'])

    # Return the result
    if result['ret'] is True:
        module.exit_json(msg=to_native(result), JobState=to_native(result['JobState']))

    else:
        module.fail_json(msg=to_native(result))

def main():
    run_module()


if __name__ == '__main__':
    main()


