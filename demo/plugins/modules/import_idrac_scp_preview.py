#!/usr/bin/python
# Copyright: (c) 2020, 
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: import_idrac_scp_preview

short_description: Import a given idrac server configuration profiles without making any changes to target host (preview).

version_added: -

description: This module previews an idrac server configuration profile from a network share on host(s) by utilizing Redfish API, specifically the OEM action EID_674_Manager.ImportSystemConfigurationPreview. It returns an "Accepted" or "Error" message and a job_id. This can be used to test import of an SCP on multiple hosts safely without making any changes. Currently only supports network share type NFS.

notes:
    - Run on a machine with direct access to DellEMC iDRAC
    - Use get_job_details module to get details about the failure (attributes which failed to import).
    - Manager instance id "iDRAC.Embedded.1" is hardcoded into the code. Should be patched for best practise and accept the id 
      as parameter or traverse the manager members itself.
options:
    idrac_ip:
        description: IP address of the OOB controller
        required: true
        type: str
    idrac_user:
        description: Username for authentication with the OOB controller
        required: true
        type: str
    idrac_password:
        description: Password for authentication with the OOB controller
        required: true
        type: str
    share_type:
        description: Share type to import configuration from
        required: true
        type: str
        accepted values: NFS
    share_name:
        description: Network share path
        required: true
        type: str
    scp_file:
        description: Name of the SCP file
        required: true
        type: str
    scp_components:
        description: If ..
          - ALL: import all component configurations from the SCP file
          - IDRAC: import iDRAC configuration from the SCP file
          - BIOS: import BIOS configuration from the SCP file
          - NIC: import NIC configuration from SCP file
          - RAID: import RAID configuration from SCP file
        required: true
        type: str
'''

EXAMPLES = r'''
# Example run

- name: Import all configurations from SCP file
  import_idrac_scp_preview:
    idrac_ip: "{{ ansible_host }}"
    idrac_user: "{{ ansible_user }}"
    idrac_password: "{{ ansible_password }}"
    share_ip: { share ip }
    share_name: "/nfs"
    share_type: "NFS"
    scp_file: { scp file name }
    scp_components: "ALL"

'''

RETURN = r'''
Return values coming soon.
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import open_url
from ansible.module_utils.six.moves.urllib.error import URLError, HTTPError
from ansible.module_utils._text import to_native
import json
from ansible_collections.community.general.plugins.module_utils.redfish_utils import RedfishUtils
import re
import time

class ExtendedRedfishUtils(RedfishUtils): 
    def import_config_preview(self):
        result = {} 
        action_uri = "iDRAC.Embedded.1/Actions/Oem/EID_674_Manager.ImportSystemConfigurationPreview"
        full_uri = self.root_uri + action_uri 
        
        post_payload = {
            'ShareParameters':
            {
            'ShareType': self.module.params['share_type'],
            'ShareName': self.module.params['share_name'],
            'IPAddress': self.module.params['share_ip'],
            'FileName': self.module.params['scp_file'],
            'Target': self.module.params['scp_components']
            } 

        }  
        response = self.post_request(full_uri, post_payload)
        result['ret'] = response['ret']
        if response['ret'] is False:
            result['Message'] = response['msg'] 
            return result
        
        # Identify job id
        response_output = response['resp'].__dict__
        headers_loc = response_output["headers"]["Location"]
        job_id = re.search("JID_.+", headers_loc).group()
        
        # Fill result dict
        result['Message'] = "Import SCP preview job created"
        result['job_id'] = job_id  
             
        return result
 
def run_module():

    # define result dict
    result = {}
    # the AnsibleModule object will be our abstraction working with Ansible
    module = AnsibleModule(
        argument_spec = dict(
            # defaults
            timeout = dict(required=False, type='int', default = 10),
            #idrac credentials
            idrac_ip = dict(required=True, type='str'),
            idrac_user = dict(required=True, type='str'),
            idrac_password = dict(required=True, type='str', no_log=True), 
            # network share 
            share_ip = dict(required=True, type='str'),
            share_name = dict(required=True, type='str'),
            share_type = dict(required=True, type='str'),
            scp_file = dict(required=True, type='str'),
            scp_components = dict(required=False, choices=['ALL', 'IDRAC', 'BIOS', 'NIC', 'RAID'], default='ALL'), 
        ),
        supports_check_mode=False)
        
    # credentials 
    creds = {'user': module.params['idrac_user'],
             'pswd': module.params['idrac_password']}
    # Build URL
    root_uri = "https://" + module.params['idrac_ip'] + "/redfish/v1/Managers/"
    
    # create RedfishUtils object
    rf_utils = ExtendedRedfishUtils(creds, root_uri, module.params['timeout'],module)     
   
    result = rf_utils.import_config_preview()
    
    # Return #TODO: Error handling
    if result['ret'] is True:
        module.exit_json(msg=to_native(result),job_id=to_native(result['job_id']))
 
    else: 
        module.fail_json(msg=to_native(result))

def main():
    run_module()


if __name__ == '__main__':
    main()


