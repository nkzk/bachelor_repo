# Demo

This directory is a slightly modified version of the [redfish-ansible](../redfish-ansible) directory. A number of files and playbooks has been modified to better suit the demonstration where credentials and settings are changed rapidly.



This document contains the script used for the demonstration part of the presentation to the employer. It includes example curl commands to interact with the Redfish API, and commands used to run the playbooks from the redfish-ansible directory.

Contents:

- Redfish examples:
   - Write settings to iDRAC
   - Write settings to BIOS
   - Read and save iDRAC settings using Redfish
- Ansible examples:
   - Simulate deployment of settings on "out-of-the-box" servers
       - Write iDRAC settings
       - Writing BIOS settings with Redfish/Ansible
   - Read and save iDRAC settings
   - Read and save BIOS settings


## Redfish examples
### Write settings to iDRAC
To write settings to the iDRAC we need to make a PATCH request on the "Attributes" resource of an iDRAC instance. A PATCH request will only overwrite the attributes we specified.
For this example we want to change the iDRACs default root credentials and enable serial over lan for the root user.

First we check the default settings, serial over lan for the root user should be disabled:
```
curl -X GET -u root:calvin -k \
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
| jq .Attributes | grep -e "Users.2.SolEnable"
```
Then we configure the iDRAC with a PATCH request:
```
curl -X PATCH -u root:calvin -k \
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
-H "content-type: application/json" \
--data '{"Attributes": {"Users.2.SolEnable": "Enabled", "Users.2.UserName": "redfish", "Users.2.Password": "redfish_new" }}' | python -m json.tool
```
To test if the settings truly was applied we can try to make a GET request on the redfish service entry using the previous credentials: root:calvin

```
curl -X GET -u root:calvin -k \
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
| jq .Attributes | grep -e "Users.2.SolEnable"
```
Didnt work -> change credentials -> see that value of SolEnable has changed.


### Write settings to BIOS
Lets say we want to disable hyperthreading through the BIOS settings. We do this by making another PATCH request, this time on the /Bios/Settings URI of an instance of the Systems resource.
Based on iDRAC documentation, i know that the "LogicalProc" attribute is the iDRAC variable which enables or disables hyper-threading.
First lets check that the LogicalProc attribute is Enabled, as it is by default.
```
curl -X GET -u redfish:redfish\_new -k https://192.168.0.125/redfish/v1/Systems/System.Embedded.1/Bios | jq .Attributes | grep LogicalProc
```
We craft a PATCH request in the same manner as we did when writing settings to the iDRAC, in the payload we include the attribute name (logicalproc) and the desired value which should be disabled.

```
curl -sX PATCH -u redfish:redfish\_new -k \
https://192.168.0.125/redfish/v1/Systems/System.Embedded.1/Bios/Settings\
 --data '{"Attributes": {"LogicalProc": "Disabled" }}' \
-H "content-type: application/json"
```

Now we have to do an IDRAC specific thing. We have to create a BIOS configuration job before rebooting the system. This is just something you either know or learn after countless attempts and wondering why the settings wont apply. On other vendors you dont have to go through this step.

You create a BIOS configuration job by making a POST request on the jobs resource on the iDRAC, we include the -v parameter for verbose output, which will include the headers of the response. In the headers there is a location attribute with the location of the job created, which we can use later to monitor the progress of the BIOS configuration job.

```
curl -sX POST -u redfish:redfish_new -k -v \
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Jobs \
--data '{"TargetSettingsURI": "/redfish/v1/Systems/System.Embedded.1/Bios/Settings"}' \
-H "content-type: application/json"
```

Now we can reboot the system with a POST request on the system instance to initiate the bios configuration job.


```
curl -X POST -v -u redfish:redfish_new -k \
https://192.168.0.125/redfish/v1/Systems/System.Embedded.1/Actions/ComputerSystem.Reset \
--data '{"ResetType": "ForceRestart" }' \
-H "content-type: application/json"
```

To monitor the job-progress, we make a GET request on the URI we wrote down when creating the job earlier.

```
curl -X GET -u root:calvin -k https://192.168.0.123/\<Location-URI\> \
| python -m json.tool
```
This could however take a few minutes, and we are not going to wait for it, but after the job has completed, the bios config should have been applied.

After the job is complete we can check if the setting has been successfully changed with another GET request:

```
curl -X GET -u root:calvin -k \
https://192.168.0.125/redfish/v1/Systems/System.Embedded.1/Bios | jq .Attributes | grep LogicalProc
```

### Read and save iDRAC settings using Redfish
To get a list of all the settings attributes on the iDRAC, we make a GET request on the 'Attributes' resource of the iDRAC instance, and then pipe the request to the jq-tool which extracts the "Attributes" data of the response.
```
curl -X GET -u redfish:redfish_new -k \
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
| jq .Attributes
```
The command output can be redirected to a file, effectively saving a json-object with all the attributes of the iDRAC.


### Read and save BIOS settings using Redfish
We can do the same with BIOS, except this time it is a request on the Bios resource of a system instance: in this case, system.embedded.1
We can save the response to a file exactly the same way as we did with the iDRAC settings.
```
curl -X GET -u redfish:redfish\_new -k \
https://192.168.0.125/redfish/v1/Systems/System.Embedded.1/Bios \
 | jq .Attributes > bios\_settings.json
```

## Ansible:
### Simulate a deployment of iDRAC settings on "out-of-the-box" servers
Change cwd to 'redfish-ansible/'

To save some time, instead of resetting iDRAC and BIOS, we just revert the changes so that we have the default user credentials and settings to simulate the deployment of settings on a 'fresh' server. In this case we want to simulate the deployment of iDRAC settings on two "fresh" servers simultaniously.

Reverting changes:
```
https://192.168.0.125/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
-H "content-type: application/json" \
--data '{"Attributes": {"Users.2.SolEnable": "Disabled", "Security.1.MinimumPasswordScore": "No Protection", "Users.2.UserName": "root", "Users.2.Password": "calvin" }}' | python -m json.tool

curl -X PATCH -u root:calvin -k \
https://192.168.0.123/redfish/v1/Managers/iDRAC.Embedded.1/Attributes \
-H "content-type: application/json" \
--data '{"Attributes": {"Users.2.SolEnable": "Disabled", "Security.1.MinimumPasswordScore": "No Protection", "Users.2.UserName": "root", "Users.2.Password": "calvin" }}' | python -m json.tool
```

First we define a group in our inventory file:
```
vim inventory/static\_inventory.yml
```

We include the two idrac9s in the demo-group.

Now lets look at the playbook.
```
cat playbooks/bmc\_settings.yml
```
[Talk a little about it, segway into roles:]

role for idrac\_settings:

```
cat roles/idrac\_settings/tasks/main.yml
```
talk about the idrac\_redfish\_config module 
    uses the Redfish API to apply the settings that we define when calling the module.
then print output to console


Seing it in action:
```
ansible-playbook -i inventory playbooks/bmc_settings.yml
```

And thats how easy it is to apply specific settings to multiple idracs. 
In the debug output we can see a recap of all the attributes that we applied, and we got no errors so the settings should be applied.

if want to verify (can skip):
```
curl -sX GET -u root:calvin -k https://192.168.0.123/redfish/v1

curl -sX GET -u root:calvin -k https://192.168.0.125/redfish/v1

try redfish:redfish_new :)
```

### Writing BIOS settings
Now lets try to do the same but with BIOS settings:
We are going to disable hyperthreading on both the iDRAC9 servers which are in the demo-group.

the tasks that change bios settings are also grouped together in a seperate role 
```
vim roles/bios_idrac_settings/tasks/main.yml
```
We are using 3 different Ansible Redfish-modules here:
    redfish_config to que the changes
    idrac_redfish_command to create a bios configuration job which we mentioned earlier was a requirement for iDRAC.
    redfish_command to restart the system which will initiate the job that applies the changes

running the playbook:
```
ansible-playbook -i inventory/ playbooks/bios_settings.yml
```

Because the idrac\_redfish\_command module does not return a job\_id, we have to find the job\_id ourselves to monitor the progress of the configuration job. Supposedely a patch is planned, but if it never comes,  a fork can also be made if this functionality is important. After modifying the functionality you can use the "get\_job\_id\_details" module in [modules](redfish-ansible/plugins/modules) which is an example module we wrote which gets information about a job.


Manually finding the created job:
Last entry in the output of this command:
```
curl -X GET -u redfish:redfish_new -k https://192.168.0.123/redfish/v1/Managers/iDRAC.Embedded.1/Jobs | python -m json.tool
```
Do a GET request on the URI of the last entry.



### Demonstrate reading settings 

#### Reading idrac settings and store to file
```
ansible-playbook -i inventory/ playbooks/demo_get_idrac_info.yml
```

#### Reading bios settings and store to file
```
ansible-playbook -i inventory/ playbooks/demo_get_bios_settings.yml
```



--------------------------------------------------------------------------------

# This part was intended for the demonstration, but was cut out.

It builds on the iDRAC settings we retrieved with the Redfish "Reading iDRAC settings"-example, and how the file which contains all iDRAC attributes could be pushed to another server.
Includes python code example.

## Deploy a �default� iDRAC based on previously gathered settings using Redfish
Our iDRAC9 with ip ending in 123 has all the default settings. We want to use to settings that we extracted from the previous examples and apply them on this server. There are a couple of things we need to think about though. Most of the node-specific settings like MAC-address and current IP is read-only, so they can safely be included. However, we have to remove any IP attributes that may cause conflicts in the network. 
```
vim settings.json  # file created from the redirected std output when getting iDRAC setttings
```
search for IPv4, remove settings that are write-able and may cause network-conflicts

deally, you'd only include the specific attributes that you want to apply, but for demonstration purposes, let's say we have configured alot of idrac settings on 1 server, and just want to dump it all on another iDRAC. This is possible because iDRAC settings which are read-only, will not be applied, and wont break the process of applying the configuration.

Because the settings are in a file, i'm going to use python to help craft the payload, but you could use any script or programming language.

```
#!/usr/bin/python
import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning

jsonfile = open("settings_changed.json",)
attributes=json.load(jsonfile)

url = 'https://192.168.0.123/redfish/v1/Managers/iDRAC.Embedded.1/Attributes'
payload = json.dumps({"Attributes": attributes})
request = requests.patch(url, auth=('root','calvin'), headers={'content-type': 'application/json'}, data=payload,verify=False)

print("Response code: ", request.status_code)
if request.status_code == 200:
    print("Success")
    print(request.content())
else:
    print("Failed")
    print(request.content())


```


As you can see we only use two basic libraries: requests for HTTP requests, and "json" to parse json data. 
So basically we use python to help craft the payload which consists of the key "Attributes" and the contents of the settings-file we previously created.


Save and run the script.
We return the content of the response even if success. A bunch of results which looks like errors may appear. These are all the settings that were read-only that were included in the settings.json-file but failed to apply when running the script. This is ok, cause any settings that were writeable and did not depend on any other attributes has been applied.





